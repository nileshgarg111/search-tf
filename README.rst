Search Optimization from TensorFlow


Creating Enviroment in Python
==============================

python3 -m venv [Env_Name]
source [Env_Name]/bin/activate

For Leaving Virtual Env : deactivate



Install PM2
===========

 - Install pm2 for running python app
 - run : pm2 start app.py --name "searchTF"
 - For Logs : pm2 logs



Commands in Start
====================
pip install tensorflow-gpu==1.13.1
pip install tensorflow-hub
pip install tf-sentencepiece

sudo apt-get install libpq-dev python3-dev libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev libffi-dev

pip install simpleneighbors
pip install tqdm

pip install waitress

Creation of Annoy File
========================
Setup Cron from createAnnoyCron.sh file
Or by running command nohup python createAnnoyFile.py > /var/www/logs/annoyFileCreationLog.log &