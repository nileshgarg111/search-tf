
from flask import Flask, make_response, request, jsonify
import configs

import tensorflow as tf
import tensorflow_hub as hub
from simpleneighbors import SimpleNeighbors
import tf_sentencepiece
import time

app = Flask(__name__)

index_files = {}
index = SimpleNeighbors.load(configs.ANNOY_FILE_NAME_PREFIX) #Index Load

for months_num in configs.SEARCH_INDEXING_MONTHS_FILE:
  annoy_file_name = configs.ANNOY_FILE_NAME_PREFIX + str(months_num)
  index_files[months_num]  = SimpleNeighbors.load(annoy_file_name)
  print("Loaded file - ", annoy_file_name)

#Session Creation start
module_url = 'https://tfhub.dev/google/universal-sentence-encoder-multilingual/1'
g = tf.Graph()
with g.as_default():
    text_input = tf.placeholder(dtype=tf.string, shape=[None])
    embed = hub.Module(module_url)
    embedded_text = embed(text_input)
    init_op = tf.group([tf.global_variables_initializer(), tf.tables_initializer()])
g.finalize()
session = tf.Session(graph=g)
session.run(init_op)
#Session Creation end


# handle undefined routes
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

#Search Result Ids. [POST] Request
@app.route(configs.API_VERSION_PATH + '/search-results', methods=['POST'])
def search_results():
    if not request.json or not 'query' in request.json:
            return make_response(jsonify({'error': 'Bad Request'}), 400)

    #Query Building and result
    sample_query = request.json['query']
    num_results = int(request.json['count']) if ('count' in request.json)  else configs.SEARCH_RESULT_DEFAULT_COUNT
    time_months = int(request.json['time_months']) if ('time_months' in request.json)  else configs.DEFAULT_SEARCH_INDEXING_MONTHS_FILE
    query_embedding = session.run(embedded_text, feed_dict={text_input: [sample_query]})[0]
    search_results = index_files[time_months].nearest(query_embedding, n=num_results)
    return jsonify(search_results), 200

#Similar Content API. [POST] Request
@app.route(configs.API_VERSION_PATH + '/similar-content', methods=['POST'])
def similar_content():
    if not request.json or not 'id' in request.json:
            return make_response(jsonify({'error': 'Bad Request'}), 400)

    #Query Building and result
    num_results = int(request.json['count']) if ('count' in request.json)  else configs.SIMILAR_CONTENT_DEFAULT_COUNT
    time_months = int(request.json['time_months']) if ('time_months' in request.json)  else configs.DEFAULT_SIMILAR_CONTENT_MONTHS_FILE
    similar_content = index_files[time_months].neighbors(request.json['id'], n=num_results)
    return jsonify(similar_content), 200

if __name__ == '__main__':
    from waitress import serve
    serve(app, host=configs.HOST, port=configs.PORT)

