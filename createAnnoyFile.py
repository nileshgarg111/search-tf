#nohup python createAnnoyFile.py > /var/www/logs/annoyFileCreationLog.log &

import tensorflow as tf
import tensorflow_hub as hub
import tf_sentencepiece  # Not used directly but needed to import TF ops.

from simpleneighbors import SimpleNeighbors
import time
import json
import hashlib
from pymongo import MongoClient
import datetime
from dateutil.relativedelta import relativedelta
import configs

print("Script to create annoy file started at",time.strftime("%H:%M:%S", time.localtime()))

#Connect to Mongo and get the articles data and added to array
#MONGO_HOST = "localhost" #Localhost
MONGO_HOST = "172.30.0.156" #PROD
#MONGO_HOST = "172.30.0.232" #UAT Mongo Public IP is - 52.20.12.121
MONGO_DB = "Skimai"
COLLECTION_NAME = "articledatas"
client = MongoClient(MONGO_HOST, 27017)
db = client[MONGO_DB]

#Batch variables
BATCH_SIZE_TO_PROCESS = 3000

#Session Creation
module_url = 'https://tfhub.dev/google/universal-sentence-encoder-multilingual/1'

g = tf.Graph()
with g.as_default():
  text_input = tf.compat.v1.placeholder(dtype=tf.string, shape=[None])
  embed = hub.Module(module_url)
  embedded_text = embed(text_input)
  init_op = tf.group([tf.compat.v1.global_variables_initializer(), tf.compat.v1.tables_initializer()])
g.finalize()

session = tf.compat.v1.Session(graph=g)
session.run(init_op)


for months_num in configs.SEARCH_INDEXING_MONTHS_FILE:
  batch = []
  batch_titles = []
  batch_start_ends = []
  seen_hashes = set()
  lines = []
  i = 0
  months_before = (-1 * months_num)
  annoy_file_name = configs.ANNOY_FILE_NAME_PREFIX + str(months_num)
  date_calc = datetime.datetime.today() + relativedelta(months=months_before)
  db_data = db[COLLECTION_NAME].find({"publishedDate" : {"$gte" : date_calc}, "language" : "en", "toAddInSearchFlag" : True})
  print("Started creating file ",annoy_file_name, " after date ", date_calc, " at ", time.strftime("%H:%M:%S", time.localtime()))

  index = SimpleNeighbors(512, metric='angular')
  print("Batch Processing started at",time.strftime("%H:%M:%S", time.localtime()))
  for doc in db_data:
      if 'sentencesJssoup' in doc:
        sentences = [doc['title']] + doc['sentencesJssoup']
      else:
        sentences = [doc['title']] + doc['articleData']
      hashy = hashlib.sha224(' '.join(sentences).encode()).hexdigest()

      if hashy in seen_hashes:
          continue
      seen_hashes.add(hashy)

      start = len(batch)
      end = start + len(sentences)
      batch_start_ends.append((start,end))
      batch_titles.append(str(doc['_id'])) #Field to be used for indexing purpose
      batch.extend(sentences)

      if len(batch) > BATCH_SIZE_TO_PROCESS:
          i=i+1
          print("Processing batch",i,"of size",BATCH_SIZE_TO_PROCESS,"at",time.strftime("%H:%M:%S", time.localtime()))
          vecs = session.run(embedded_text, feed_dict={text_input: batch})
          for id, (s,e) in zip(batch_titles, batch_start_ends):
              doc_vec = vecs[s:e].mean(0)   # Average sentence vecs to get doc vec
              index.add_one(id, doc_vec) # Use title as ID
          batch = []
          batch_start_ends = []
          batch_titles = []

  if len(batch) > 0:
      print("Processing remaining elements",len(batch),"at",time.strftime("%H:%M:%S", time.localtime()))
      vecs = session.run(embedded_text, feed_dict={text_input: batch})
      for id, (s,e) in zip(batch_titles, batch_start_ends):
          doc_vec = vecs[s:e].mean(0)   # Average sentence vecs to get doc vec
          index.add_one(id, doc_vec) # Use title as ID
  len(seen_hashes)
  print("Batch Processing ends at",time.strftime("%H:%M:%S", time.localtime()))
  n_trees = 100
  index.build(n=n_trees)
  index.save(annoy_file_name)
  print("File - ",annoy_file_name, " created at  ", time.strftime("%H:%M:%S", time.localtime()))
print("Script to create annoy file ended at",time.strftime("%H:%M:%S", time.localtime()))
