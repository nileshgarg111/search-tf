#!/bin/sh
now=$(date +"%m_%d_%Y")

cd /var/www/html/search-tf
. searchTF/bin/activate
nohup python createAnnoyFile.py > /var/www/logs/annoyFileCreation$now.log
sudo su
pm2 restart searchTF
#nohup python createAnnoyFile.py > /var/www/logs/annoyFileCreation$now.log & #For Local
